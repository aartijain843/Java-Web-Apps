# This file is a template, and might need editing before it works on your project.
FROM maven:3.5-jdk-11 as BUILD
COPY target/*.jar /
EXPOSE 8080
ENTRYPOINT ["java","-jar","/webapp.jar"]

